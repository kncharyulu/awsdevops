#Colections ...Counter
#from collections import Counter
#x = "aaaaabbbbccc"
#mycounter = Counter(x)
#print(mycounter)
#print(list(mycounter.elements()))

#Colections ...namedtuple
#from collections import namedtuple
#Point = namedtuple('Point','x,y')
#pt = Point(1,-4)
#print(pt.x,pt.y)

#Colections ...OrderedDict
#from collections import OrderedDict
#ordereddict = OrderedDict{}
#ordereddict['b'] = 2
#ordereddict['d'] = 4
#ordereddict['c'] = 3
#ordereddict['a'] = 1
#print(ordereddict)

#Colections ...OrderedDict
from collections import defaultdict
d = defaultdict(int)
d['a'] = 1
d['b'] = 2
print(d['c'])

#Colections ...deque
#from collections import deque
#d = deque()
#d.append(1)
#d.append(2)
#d.appendleft((3))
#print(d)
#d.extendleft((4,5,6))
#print(d)
#d.rotate(1)
#print(d)
#d.rotate(-1)
#print(d)

